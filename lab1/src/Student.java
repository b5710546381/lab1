//package oop.lab1;

//TODO Write class Javadoc
/**
 * A Student inherits from Person with id
 * @author Pakpon Jetapai 
 * @version 2015.01.13
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**Initialize a ne�w Student.
	 * @param name is the name of Student
	 * @param id is the identity of Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}
	//TODO Write equals
	/**
	 * Compare student by id.
 * They are equal if the id matches.
	 * @param other is another Student to compare to this one.
	 * @return true if the id is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		System.out.println("This is from Student class");
		if (obj == null) return false;
		if (obj.getClass() != this.getClass()) return false;
		Student other = (Student) obj;
		if(getName().equalsIgnoreCase(other.getName())) return true;
		return false;
	}
}
